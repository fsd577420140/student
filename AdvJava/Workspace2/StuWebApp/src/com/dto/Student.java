package com.dto;

public class Student {
	private int stuId;
	private String stuName;
	private double Fee;
	private String gender;
	private String emailId;
	private String password;
	
	public Student() {
		super();
	}

	public Student(int stuId, String stuName, double Fee, String gender, String emailId, String password) {
		super();
		this.stuId = stuId;
		this.stuName = stuName;
		this.Fee = Fee;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getStuId() {
		return stuId;
	}
	public void setStuId(int stuId) {
		this.stuId = stuId;
	}

	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public double getFee() {
		return Fee;
	}
	public void setFee(double Fee) {
		this.Fee = Fee;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + stuId + ", empName=" + stuName + ", salary=" + Fee + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
}
