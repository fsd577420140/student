<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.List, com.dto.Student"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllStudents</title>
</head>
<body>

	<jsp:include page="AdminHomePage.jsp" />

	<table border="2" align="center">

		<tr>
			<th>stuId</th>
			<th>stuName</th>
			<th>Fee</th>
			<th>Gender</th>
			<th>Email-Id</th>
			<th colspan="2">Actions</th>
		</tr>

		<c:forEach var="stu" items="${stuList}">
		<tr align="center">
			<td> ${stu.stuId}  </td>
			<td> ${stu.stuName}</td>
			<td> ${stu.fee} </td>
			<td> ${stu.gender} </td>
			<td> ${stu.emailId}</td>
			<td> <a href='EditStudent?stuId=${stu.stuId}'>Edit</a> </td>
			<td> <a href='DeleteStudent?stuId=${stu.stuId}'>Delete</a> </td>
		</tr>
		</c:forEach>

	</table>

</body>
</html>
