package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dto.Student;


@WebServlet("/Profile")
public class Profile extends HttpServlet {
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		Student stu = (Student) session.getAttribute("stu");
		
		//RequestDispatcher rd = request.getRequestDispatcher("EmpHomePage");
		//rd.include(request, response);
		
		//OR
		
		request.getRequestDispatcher("StuHomePage").include(request, response);
		
		out.println("<table border='2' align='center'>");

		out.println("<tr>");
		out.println("<th>StuId</th>");
		out.println("<th>StuName</th>");
		out.println("<th>Fee</th>");
		out.println("<th>Gender</th>");
		out.println("<th>Email-Id</th>");
		out.println("<th>Password</th>");
		out.println("</tr>");

		out.println("<tr>");
		out.println("<td>" + stu.getStuId()   + "</td>");
		out.println("<td>" + stu.getStuName() + "</td>");
		out.println("<td>" + stu.getFee()  + "</td>");
		out.println("<td>" + stu.getGender()  + "</td>");
		out.println("<td>" + stu.getEmailId() + "</td>");
		out.println("<td>" + stu.getPassword()+ "</td>");
		out.println("</tr>");

		out.println("</table>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
