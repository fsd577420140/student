package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.dto.Student;


@WebServlet("/GetAllStus")
public class GetAllStus extends HttpServlet {
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		StudentDAO stuDao = new StudentDAO();		
		List<Student> stuList = stuDao.getAllStudents();
		
		RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage");
		rd.include(request, response);
		
		out.println("<center>");
		
		if (stuList != null) {
			
			out.println("<table border=2>");
			
			out.println("<tr>");
			out.println("<th>StuId</th>");
			out.println("<th>StuName</th>");
			out.println("<th>Fee</th>");
			out.println("<th>Gender</th>");
			out.println("<th>Email-Id</th>");
			out.println("</tr>");
			
			for (Student stu : stuList) {
				out.println("<tr>");
				out.println("<td>" + stu.getStuId()   + "</td>");
				out.println("<td>" + stu.getStuName() + "</td>");
				out.println("<td>" + stu.getFee()  + "</td>");
				out.println("<td>" + stu.getGender()  + "</td>");
				out.println("<td>" + stu.getEmailId() + "</td>");
				out.println("</tr>");
			}
			
			out.println("</table>");			
		
		} else {			
			out.println("<h1 style='color:red;'>Unable to Fetch the Student Record(s)!!!</h1>");	
		}
		out.println("</center>");
	}


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
