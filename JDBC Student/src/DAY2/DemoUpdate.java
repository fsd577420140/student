package DAY2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

//Update student Salary
public class DemoUpdate {
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;

		System.out.println("Enter Student Id and New Fees");
		Scanner scan = new Scanner(System.in);
		int stuId = scan.nextInt();;
		double fees = scan.nextDouble();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		
		String updateQuery = "update student set fee = " + fees + " where stuId = " + stuId;
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			int result = stmt.executeUpdate(updateQuery);

			if (result > 0) {
				System.out.println("student Record Updated");
			} else {
				System.out.println("Failed to Update the student Record!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
