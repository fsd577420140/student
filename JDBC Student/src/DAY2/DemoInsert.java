package DAY2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DemoInsert {
    public static void main(String[] args) {

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        System.out.println("Enter Student Id, Name, Fees, Gender");
        Scanner scan = new Scanner(System.in);

        // Read the entire line and then parse the input as needed
        int stuId = Integer.parseInt(scan.nextLine());
        String stuName = scan.nextLine();
        double fees = Double.parseDouble(scan.nextLine());
        String gender = scan.nextLine();

        System.out.println();

        String url = "jdbc:mysql://localhost:3306/fsd57";

        String insertQuery = "insert into student values " +
                "(" + stuId + ", '" + stuName + "', " + fees + ", '" + gender +
                "')";

        String fetchQuery = "select * from student";

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, "root", "root");

            stmt = con.createStatement();
            int result = stmt.executeUpdate(insertQuery);
            rs = stmt.executeQuery(fetchQuery);

            if (result > 0) {
                System.out.println("Student Record Inserted");
            } else {
                System.out.println("Failed to Insert the Student Record!!!");
            }
            if (rs.next()) {
                do {
                    System.out.print(rs.getInt(1) + "    " + rs.getString(2) + "    ");
                    System.out.println(rs.getDouble(3) + "    " + rs.getString(4) + "    ");

                } while (rs.next());
            } else {
                System.out.println("Student Record(s) Not Found!!!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            // Close resources in the finally block
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
