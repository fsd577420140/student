package DAY2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

//Fetch student Data based on studentId: select * from student where stuId=101
public class Demoselect {
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		System.out.print("Enter Employee Id: ");
		int stuId = new Scanner(System.in).nextInt();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from student where stuId = " + stuId;
		
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				System.out.println("StudentId   : " + rs.getInt(1));
				System.out.println("StudentName : " + rs.getString("stuName"));
				System.out.println("Fees  : " + rs.getDouble(3));
				System.out.println("Gender  : " + rs.getString("gender")+"\n");
				
			} else {
				System.out.println("Employee Record Not Found!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}