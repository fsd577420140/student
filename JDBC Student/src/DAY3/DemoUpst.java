package DAY3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.Dbconnection;

public class DemoUpst {
	public static void main(String[] args) {
		
		Connection con = Dbconnection.getConnection();
		PreparedStatement pst = null;
	
		System.out.println("Enter Student Id and New Fees");
		Scanner scan = new Scanner(System.in);
		int stuId = scan.nextInt();;
		double fee = scan.nextDouble();
		System.out.println();
		
		String updateQuery = "update student set fee = (?) where stuId = (?) " ;
		
		try {
			
			pst = con.prepareStatement(updateQuery);
			pst.setInt(2, stuId);
			pst.setDouble(1,fee);
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Student Record Updated!!!");
			} else {
				System.out.println("Failed to Update the Student Record!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}		
	}
}